from django.contrib import admin
from CoronApp.models import *

# Register your models here.

admin.site.register(Profil)
admin.site.register(Demande)
admin.site.register(Reponse)
admin.site.register(Retour)
