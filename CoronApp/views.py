from crispy_forms.layout import Submit
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.db import IntegrityError
from django.http import HttpResponse, JsonResponse, request, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
import json

# Create your views here.
from django.template.response import TemplateResponse
from django.urls import reverse, reverse_lazy
from django.views.decorators.http import require_GET, require_POST
from django.views.generic import ListView, DetailView
from django.views.generic import ListView, DetailView, UpdateView, CreateView

from .forms import DemandeForm, RetourForm
from .models import *


class ListDemandes(LoginRequiredMixin, ListView):
    login_url = '/accounts/login'
    model = Demande
    paginate_by = 100
    template_name = 'demande/list-demandes.html'
    queryset = Demande.objects.all()
    context_object_name = 'demandes'


class DemandeView(LoginRequiredMixin, DetailView):
    login_url = '/accounts/login'
    model = Demande
    template_name = 'demande/detail-demande.html'
    context_object_name = 'demande'


@require_GET
def assign_view(request, pk, t):
    if request.user.is_authenticated:
        r = Reponse.objects.get(demande__id=pk)
        if t == "vehicule":
            if request.user.has_perm('CoronApp.can_assign_vehicules'):
                return render(request, 'assignements/assignement-vehicules.html', {'reponse': r})
        if t == "subsistance":
            if request.user.has_perm('CoronApp.can_assign_subsistances'):
                return render(request, 'assignements/assignement-subsistances.html', {'reponse': r})
        if t == "effectif":
            if request.user.has_perm('CoronApp.can_assign_effectifs'):
                return render(request, 'assignements/assignement-effectifs.html', {'reponse': r})
        if t == "logistique":
            if request.user.has_perm('CoronApp.can_assign_logistique'):
                return render(request, 'assignements/assignement-logistique.html', {'reponse': r})
        if t == "comment":
            if request.user.has_perm('CoronApp.can_comment'):
                return render(request, 'assignements/assignement-commentaire.html', {'reponse': r})

    raise PermissionDenied


@require_POST
def set_assign(request, pk):
    if request.user.is_authenticated:
        parsed_json = (json.loads(request.body))
        print(parsed_json)
        if parsed_json['type'] == 'vehicule':
            if request.user.has_perm('CoronApp.can_assign_vehicules'):
                r = Reponse.objects.get(demande_id=pk)
                r.vehicules = parsed_json['pld']
                if parsed_json['end'] == True:
                    r.provisionneVehicule()
                r.save()
                return HttpResponse(status=201)

        if parsed_json['type'] == 'subsistance':
            if request.user.has_perm('CoronApp.can_assign_subsistances'):
                r = Reponse.objects.get(demande_id=pk)
                r.subsistances = parsed_json['pld']
                if parsed_json['end'] == True:
                    r.provisionneSubsistance()
                r.save()
                return HttpResponse(status=201)

        if parsed_json['type'] == 'effectif':
            if request.user.has_perm('CoronApp.can_assign_effectifs'):
                r = Reponse.objects.get(demande_id=pk)
                r.effectifs = parsed_json['pld']
                if parsed_json['end'] == True:
                    r.provisionneEffectif()
                r.save()
                return HttpResponse(status=201)

        if parsed_json['type'] == 'logistique':
            if request.user.has_perm('CoronApp.can_assign_logistique'):
                r = Reponse.objects.get(demande_id=pk)
                r.logistique = parsed_json['pld']
                if parsed_json['end'] == True:
                    r.provisionneLogistique()
                r.save()
                return HttpResponse(status=201)

        if parsed_json['type'] == 'comment':
            if request.user.has_perm('CoronApp.can_comment'):
                r = Reponse.objects.get(demande_id=pk)
                r.commentaires = parsed_json['pld']
                r.save()
                return HttpResponse(status=201)

    raise PermissionDenied


def index(request):
    return render(request, 'index.html', context={'version' : settings.APP_VERSION})


@permission_required('CoronApp.can_validate')
def validate(request, pk):
    try:
        demande = Demande.objects.get(id=pk)
        demande.validate()
        demande.save()
        return JsonResponse({
            'success': True,
            'error': None
        })
    except ObjectDoesNotExist:
        return JsonResponse({
            'error': "La demande n'existe pas..."
        }, status=500)
    except IntegrityError:
        return JsonResponse({
            'error': "Un réponse liée à cette demande existe déja ! (Contactez un admin)"
        }, status=500)


@permission_required('CoronApp.can_refuse')
def refuse(request, pk):
    try:
        demande = Demande.objects.get(id=pk)
        demande.refuse()
        demande.save()
        return JsonResponse({
            'success': True,
            'error': None
        })
    except ObjectDoesNotExist:
        return JsonResponse({
            'error': "La demande n'existe pas..."
        }, status=500)


@permission_required('CoronApp.can_cloture')
def cloture(request, pk):
    try:
        demande = Demande.objects.get(id=pk)
        demande.cloture()
        demande.save()
        return JsonResponse({
            'success': True,
            'error': None
        })
    except ObjectDoesNotExist:
        return JsonResponse({
            'success': False,
            'error': "La demande n'existe pas..."
        })


# Doit être responsable mais c'est pas une perm
# Verifier l'identité en amont
def termine(request, pk):
    try:
        demande = Demande.objects.get(id=pk)
        demande.termine()
        demande.save()
        return JsonResponse({
            'success': True,
            'error': None
        })
    except ObjectDoesNotExist:
        return JsonResponse({
            'success': False,
            'error': "La demande n'existe pas..."
        })


class DemandeFormView(LoginRequiredMixin, CreateView):
    login_url = '/accounts/login'
    model = Demande
    form_class = DemandeForm
    template_name = 'demande/form-demande.html'
    success_url = '/demandes/'

    def get(self, request, *args, **kwargs):
        if not self.request.user.has_perm('Coronapp.add_demande'):
            raise PermissionDenied
        return super(DemandeFormView, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        if not self.request.user.has_perm('Coronapp.add_demande'):
            raise PermissionDenied
        demande = form.save(commit=False)
        demande.created_by = self.request.user
        demande.save()
        return super(DemandeFormView, self).form_valid(form)


class DemandeEditView(LoginRequiredMixin, UpdateView):
    login_url = '/accounts/login'
    model = Demande
    form_class = DemandeForm
    template_name = 'demande/form-edit-demande.html'
    success_url = '/demandes/'

    def get(self, request, *args, **kwargs):
        demande = get_object_or_404(Demande, pk=self.kwargs['pk'])

        if not ((request.user.id == demande.created_by.id and demande.etat == "cree") or (
                request.user.has_perm('CoronApp.can_edit') and demande.etat not in ["cloture", "termine"])):
            raise PermissionDenied

        form = DemandeForm(instance=demande)
        form.helper.form_action = reverse('demande-edit', kwargs={'pk': self.kwargs['pk']})

        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        demande = get_object_or_404(Demande, pk=self.kwargs['pk'])
        if (request.user.id == demande.created_by.id and demande.etat == "cree") or (
                request.user.has_perm('CoronApp.can_edit') and demande.etat not in ["cloture", "termine"]):
            return super(DemandeEditView, self).post(request, *args, **kwargs)
        raise PermissionDenied

    def get_success_url(self):
        demande = Demande.objects.get(pk=self.kwargs['pk'])
        return reverse('demande', kwargs={'pk': demande.pk})


class RetourView(LoginRequiredMixin, UpdateView):
    login_url = '/accounts/login'
    model = Retour
    form_class = RetourForm
    template_name = 'demande/form-retour.html'

    def get(self, request, *args, **kwargs):
        demande = get_object_or_404(Demande, retour__pk=self.kwargs['pk'])

        if request.user.id != demande.responsable.user.id and not request.user.is_superuser:
            raise PermissionDenied

        return super(RetourView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)

        if form.is_valid():
            demande = Demande.objects.get(retour__pk=self.kwargs['pk'])
            if request.user.id == demande.responsable.user.id or request.user.is_superuser:
                if demande.etat != 'termine':
                    demande.termine()
                    demande.save()

                return super(RetourView, self).post(request, *args, **kwargs)
            else:
                raise PermissionDenied  # Forbidden
        else:
            return HttpResponse(status=400)

    def get_success_url(self):
        retour = Retour.objects.get(pk=self.kwargs['pk'])
        return reverse('demande', kwargs={'pk': retour.demande.pk})
