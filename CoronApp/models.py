# coding=utf-8
from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django_fsm import FSMField, transition, FSMKeyField
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType

# Create your models here.


ETATS = {
    ("cree", "Créée"),
    ("valide", "Validée"),
    ("provisionne", "Provisionnée"),
    ("cloture", "Cloturée"),
    ("refuse", "Refusée"),
    ("termine", "Terminée")
}


class Profil(models.Model):
    class Meta:
        permissions = [
            ('is_chef', 'User is chef'),
        ]
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="Profil")
    tel = models.CharField(max_length=12)

    def __str__(self):
        return self.user.get_full_name()


class Demande(models.Model):
    class Meta:
        ordering = ['date_debut', 'responsable', 'titre']
        permissions = [
            ('can_validate', 'Can validate request'),
            ('can_cloture', 'Can cloture'),
            ('can_edit', 'Can edit a demand'),
            ('can_refuse', 'Can refuse a demand'),
            ('can_finish', 'Can add feedback')
        ]

    created_at = models.DateTimeField(verbose_name="Date de création de la demande", auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name="Date de dernière édition", auto_now=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)
    etat = FSMField(default="cree", choices=ETATS)  # TODO : check https://github.com/viewflow/django-fsm

    # Infos générales

    titre = models.CharField(verbose_name="Titre de la demande", max_length=128)
    date_debut = models.DateTimeField(verbose_name="Date et heure de début de l'opération")
    date_fin = models.DateTimeField(verbose_name="Date et heure de fin de l'opération")
    lieu_deroulement = models.CharField(verbose_name="Lieu de la mission", max_length=255)
    responsable = models.ForeignKey(Profil, on_delete=models.SET_NULL,
                                    null=True,
                                    verbose_name="Responsable de la mission")
    description = models.CharField(verbose_name="Description succincte", max_length=300)

    # Effectifs
    nb_benevoles = models.IntegerField(verbose_name="Nombre de bénévoles demandés", default=0)
    date_presence_debut = models.DateTimeField(verbose_name="Date et heure du rendez-vous pour les bénévoles")
    date_presence_fin = models.DateTimeField(verbose_name="Date et heure de fin de présence pour la mission")
    lieu_rdv = models.CharField(verbose_name="Lieu de rendez-vous pour les bénévoles", max_length=255)
    qualifications = models.TextField(verbose_name="Qualifications spécifiques demandées", blank=True)

    # Subsistances
    nb_repas = models.IntegerField(verbose_name="Nombre de repas nécessaires", default=0)
    lieu_repas = models.CharField(verbose_name="Lieu de la prise de repas", max_length=255, blank=True)
    commentaires_repas = models.TextField(verbose_name="Commentaires/Régimes alimentaires particuliers", blank=True)

    # Vehicules
    nb_transport = models.IntegerField(verbose_name="Bénévoles à transporter", default=0)
    nb_log = models.IntegerField(verbose_name="Nombre de LOG", default=0)
    nb_vl = models.IntegerField(verbose_name="Nombre de VL", default=0)
    nb_vpsp = models.IntegerField(verbose_name="Nombre de VPSP", default=0)
    commentaire_transport = models.TextField(verbose_name="Demande spécifique", blank=True)

    # Logistique
    nature_chargement = models.CharField(verbose_name="Nature du chargement",
                                         help_text="Taille, poids, conditionnement",
                                         max_length=255, blank=True)
    est_alimentaire = models.BooleanField(verbose_name="Contient de l'alimentaire", default=False)
    est_frais = models.BooleanField(verbose_name="Frais", default=False)
    moyens_engages = models.TextField(
        verbose_name="Moyens engagés",
        help_text="[type, nombre, utilisation], [secours, urgence (CAI/CHU/etc.), électriques ("
                  "chauffage/GPE/rallonges/lampes/etc.), soutien (tables/chaise/etc.)]", blank=True)

    # Infos complémentaires
    contact_in_situ = models.CharField(verbose_name="Contact sur place", max_length=255, blank=True)
    tel_in_situ = models.CharField(verbose_name="Téléphone du contact sur place", max_length=12, blank=True)
    description_detaillee = models.TextField(verbose_name="Description détaillée de la mission",
                                             help_text="Chronogramme précis, transport, mise en place etc.")

    @transition(field=etat, source="cree", target="valide",
                permission=lambda instance, user: user.hasPerm('CoronApp.can_validate'))
    def validate(self):
        r = Reponse(demande=self)
        r.save()

    def est_provisionne(self):
        if self.reponse.effectifs_provisionnes and self.reponse.logistique_provisionnes and self.reponse.subsistances_provisionnes and self.reponse.vehicules_provisionnes:
            return True
        return False

    @transition(field=etat, source="valide", target="provisionne", conditions=[est_provisionne])
    def provisionne(self):
        pass  # TODO Il y a quelque chose à faire ?

    @transition(field=etat, source="cree", target="refuse")
    def refuse(self):
        pass  # TODO Il y a quelque chose à faire ?

    @transition(field=etat, source="provisionne", target="cloture",
                permission=lambda instance, user: user.hasPerm('CoronApp.can_cloture'))
    def cloture(self):
        r = Retour(demande=self)
        r.save()

    @transition(field=etat, source="cloture", target="termine")
    def termine(self):
        pass # TODO Y a t'iL qUelQue ChoSe à fAirE ?

    def __str__(self):
        return "Demande " + self.titre


class Reponse(models.Model):
    class Meta:
        permissions = [
            ('can_assign_vehicules', 'Can assign vehicules'),
            ('can_assign_effectifs', 'Can assign effectifs'),
            ('can_assign_subsistances', 'Can assign subsistances'),
            ('can_assign_logistique', 'Can assign logistique'),
            ('can_comment', 'Can comment'),
        ]

    demande = models.OneToOneField(Demande, on_delete=models.CASCADE, verbose_name="Demande", rel="reponse")
    # Effectifs
    effectifs = models.TextField(verbose_name="Effectif", help_text="Nom Prenom;Nom2 Prenom2;...", blank=True)
    effectifs_provisionnes = models.BooleanField(verbose_name="Effectifs provisionnés", default=False)

    # Véhicules
    vehicules = models.TextField(verbose_name="Véhicules attribués", blank=True)
    vehicules_provisionnes = models.BooleanField(verbose_name="Véhicules provisionnés", default=False)

    # Subsitances
    subsistances = models.TextField(verbose_name="Subsistances", blank=True)
    subsistances_provisionnes = models.BooleanField(verbose_name="Subsistances provisionnées", default=False)

    # Logistique
    logistique = models.TextField(verbose_name="Logistique", blank=True)
    logistique_provisionnes = models.BooleanField(verbose_name="Logistique provisionnée", default=False)

    commentaires = models.TextField(verbose_name="Commentaires", blank=True)

    def provisionneVehicule(self):
        self.vehicules_provisionnes = True
        # TODO faire plus propre
        try:
            self.demande.provisionne()
            self.demande.save()
            print("Demande " + str(self.demande.id) + " provisionnée")
        except:
            pass

    def provisionneLogistique(self):
        self.logistique_provisionnes = True
        # TODO faire plus propre
        try:
            self.demande.provisionne()
            self.demande.save()
            print("Demande " + str(self.demande.id) + " provisionnée")
        except:
            pass

    def provisionneSubsistance(self):
        self.subsistances_provisionnes = True
        # TODO faire plus propre
        try:
            self.demande.provisionne()
            self.demande.save()
            print("Demande " + str(self.demande.id) + " provisionnée")
        except:
            pass

    def provisionneEffectif(self):
        self.effectifs_provisionnes = True
        # TODO faire plus propre
        try:
            self.demande.provisionne()
            self.demande.save()
            print("Demande " + str(self.demande.id) + " provisionnée")
        except:
            pass

    def __str__(self):
        return "Réponse " + self.demande.titre


class Retour(models.Model):
    # Pas de permissions particulières pour l'édition du retour, qui
    # est réalisée obligatoirement par le responsable de la mission

    demande = models.OneToOneField(Demande, on_delete=models.CASCADE, rel="retour")
    created_at = models.DateTimeField(verbose_name="Date de création", auto_now=True)

    # Matériel
    nb_masques_chir = models.IntegerField(verbose_name="Masques chirurgicaux consommés", default=0)
    nb_masques_FFP2 = models.IntegerField(verbose_name="Masques FFP2 consommés", default=0)
    nb_masques_textiles = models.IntegerField(verbose_name="Masques textiles consommés", default=0)
    nb_surblouses = models.IntegerField(verbose_name="Quantité de surblouses consommées", default=0)
    nb_charlottes = models.IntegerField(verbose_name="Quantité de charlottes consommées", default=0)

    # Retour d'expérience
    accident = models.BooleanField(verbose_name="Y a-t-il eu accident/maladie d'un bénévole ?", default=False)
    difficultes = models.TextField(verbose_name="Difficultés rencontrées", blank=True)
    amelioration = models.TextField(verbose_name="Axes d'amélioration", blank=True)
    commentaire = models.TextField(verbose_name="Commentaire de retour de mission", blank=True)

    def __str__(self):
        return "Retour " + self.demande.titre


