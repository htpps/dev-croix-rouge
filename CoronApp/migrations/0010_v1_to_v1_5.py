# Migration de la v1 à la V1.5 : crée les retours qui n'existent pas

from django.db import migrations


def add_retours(apps, sch_editor):
    Demande = apps.get_model('CoronApp', 'Demande')
    Retour = apps.get_model('CoronApp', 'Retour')

    for demande in Demande.objects.all():
        if not hasattr(demande, 'retour') and demande.etat == 'cloture':
            retour = Retour()
            retour.demande = demande
            retour.save()


class Migration(migrations.Migration):

    dependencies = [
        ('CoronApp', '0009_auto_20200412_2150'),
    ]

    operations = [
        migrations.RunPython(add_retours),
    ]
