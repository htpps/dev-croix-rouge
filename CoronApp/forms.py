from django import forms
from django.urls import reverse

from CoronApp.models import Demande, Retour
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Fieldset, Row, Column


class DemandeForm(forms.ModelForm):
    class Meta:
        model = Demande
        exclude = ['etat', 'created_at', 'updated_at', 'created_by']

        datePicker = forms.DateTimeInput(attrs={
            'class': 'datetime-input',
        })
        widgets = {
            'date_debut': datePicker,
            'date_fin': datePicker,
            'date_presence_debut': datePicker,
            'date_presence_fin': datePicker
        }

    def __init__(self, *args, **kwargs):
        super(DemandeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'demande-form'

        self.helper.layout = Layout(
            Fieldset(
                'Mission',
                'titre',
                Row(
                    Column('date_debut', css_class='form-group col-md-6 mb-0'),
                    Column('date_fin', css_class='form-group col-md-6 mb-0'),
                    css_class='form-row'
                ),
                'lieu_deroulement',
                'responsable',
                'description'
            ),
            Fieldset(
                'Effectifs',
                'nb_benevoles',
                Row(
                    Column('date_presence_debut', css_class='form-group col-md-6 mb-0'),
                    Column('date_presence_fin', css_class='form-group col-md-6 mb-0'),
                    css_class='form-row'
                ),
                'lieu_rdv',
                'qualifications'
            ),
            Fieldset(
                'Subsistances',
                'nb_repas',
                'lieu_repas',
                'commentaires_repas'
            ),
            Fieldset(
                'Véhicules',
                Row(
                    Column('nb_transport', css_class='form-group col-md-3 mb-0'),
                    Column('nb_log', css_class='form-group col-md-3 mb-0'),
                    Column('nb_vl', css_class='form-group col-md-3 mb-0'),
                    Column('nb_vpsp', css_class='form-group col-md-3 mb-0'),
                    css_class='form-row'
                ),
                'commentaire_transport'
            ),
            Fieldset(
                'Logistique',
                'nature_chargement',
                'est_frais',
                'est_alimentaire',
                'moyens_engages'
            ),
            Fieldset(
                'Informations complémentaires',
                'contact_in_situ',
                'tel_in_situ',
                'description_detaillee'
            )
        )

        self.helper.form_method = 'post'
        self.helper.form_action = reverse('demande-form')
        self.helper.add_input(Submit('envoyer', 'Envoyer', css_class="float-right"))


class RetourForm(forms.ModelForm):
    class Meta:
        model = Retour
        exclude = ['demande', 'created_at']

    def __init__(self, *args, **kwargs):
        super(RetourForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'retour-form'
        self.helper.form_action = reverse('retour-form', kwargs={'pk': self.instance.id})
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('envoyer', 'Terminer le retour', css_class="float-right"))

        self.helper.layout = Layout(
            Fieldset(
                'Matériel utilisé',
                Row(
                    Column('nb_masques_chir', css_class='form-group col-md-4 mb-0'),
                    Column('nb_masques_FFP2', css_class='form-group col-md-4 mb-0'),
                    Column('nb_masques_textiles', css_class='form-group col-md-4 mb-0'),
                    css_class='form-row'
                ),
                Row(
                    Column('nb_surblouses', css_class='form-group col-md-6 mb-0'),
                    Column('nb_charlottes', css_class='form-group col-md-6 mb-0'),
                    css_class='form-row'
                ),
            ),
            Fieldset(
                'Retour d\'expérience',
                'accident',
                'difficultes',
                'amelioration',
                'commentaire'
            )
        )
