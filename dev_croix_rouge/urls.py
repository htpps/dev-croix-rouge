"""dev_croix_rouge URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from CoronApp.views import DemandeView, ListDemandes, index, DemandeFormView, cloture, validate, assign_view, \
    set_assign, refuse, RetourView, DemandeEditView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('demandes/', ListDemandes.as_view(), name='demandes'),
    path('demande/<pk>', DemandeView.as_view(), name='demande'),
    path('demande/<pk>/edit', DemandeEditView.as_view(), name='demande-edit'),
    path('demande/<pk>/assign/<t>', assign_view, name='assign'),
    path('demande/<pk>/assign_ressource', set_assign, name='assign-ressource'),
    path('demande/<pk>/cloture', cloture, name='cloture'),
    path('demande/<pk>/validate', validate, name='validate'),
    path('demande/<pk>/refuse', refuse, name='refuse'),
    path('demande/<pk>/retour', RetourView.as_view(), name='retour-form'),
    path('newdemande/', DemandeFormView.as_view(), name='demande-form'),
    path('', index),
]
